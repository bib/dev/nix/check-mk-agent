{ pkgs, lib, config, options, ... }: with lib; let
  cfg = config.services.check-mk-agent;

  check-mk-agent = with pkgs; derivation {
    name = "check-mk-agent";
    builder = "${bash}/bin/bash";
    args = [ ./builder.sh ];
    src = ./check_mk_agent.linux;
    inherit coreutils bash systemd gnugrep socat
    findutils openssl zfs ethtool gnused gawk iproute;
    system = builtins.currentSystem;
  };

in {
  options.services.check-mk-agent = {
    enable = mkEnableOption "the Check-MK agent";
    port = mkOption {
      type = types.int; default = 6556;
      description = "Port number to listen on.";
    };
  };

  config = mkIf cfg.enable {
    networking.firewall.allowedTCPPorts = [ cfg.port ];
    systemd.services."check_mk_agent@" = {
      description = "Check_MK agent";
      after = [ "network.target" "check_mk_agent.socket" ];
      requires = [ "check_mk_agent.socket" ];
      serviceConfig = {
        Type = "simple";
        StandardInput = "socket";
      };
      script = "${check-mk-agent}/bin/check_mk_agent";
    };
    systemd.sockets."check_mk_agent" = {
      description = "Check_MK agent socket";
      partOf = [ "check_mk_agent.service" ];
      wantedBy = [ "sockets.target" ];
      listenStreams = [ (toString cfg.port) ];
      socketConfig = { Accept = true; };
    };
  };
}

