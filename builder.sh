set +H
set -u

export PATH="${coreutils}/bin";
PKGPATH="$PATH:${openssl}/bin:${systemd}/bin:${gnugrep}/bin:${findutils}/bin:${gnused}/bin:${zfs}/bin:${ethtool}/bin:${gawk}/bin:${iproute}/bin:${socat}/bin"

mkdir -p $out/bin

echo "#!${bash}/bin/bash" > $out/bin/check_mk_agent
echo "export PATH=${PKGPATH}" >> $out/bin/check_mk_agent
echo "export ZFS_MODULE_TIMEOUT=0" >> $out/bin/check_mk_agent
tail +2 < $src >> $out/bin/check_mk_agent

chmod +x $out/bin/check_mk_agent
